$(document).on('submit', '#form-add-pokemon', function (e) {
    e.preventDefault();

    //console.log('coucou');
    let url = site_url + 'form/validation/pokemon';
    let formData = $('#form-add-pokemon').serialize();

    $.ajax({
        url : url,
        type : 'POST',
        data : formData,
        success : function (data) {
            if (!data.success) {
                $('p.resultat').text(data.error).addClass('text-danger');
            } else {
                $('p.resultat').text(data.success).addClass('text-success');
            }
        }
    });



});