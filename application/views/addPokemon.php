<div class="container">
    <div class="row">
        <div class="col-12">
            <form id="form-add-pokemon">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="pok_name">Nom</label>
                        <input type="text" name="pok_name" class="form-control" id="pok_name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="pok_hp">HP</label>
                        <input type="number" name="pok_hp" class="form-control" id="pok_hp">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pok_description">Description</label>
                    <textarea class="form-control" name="pok_description" id="pok_description"></textarea>
                </div>
                <div class="form-group">
                    <label for="pok_img_url">URL de l'image</label>
                    <input type="text" name="pok_img_url" class="form-control" id="pok_img_url">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="pok_type_1">Type du Pokémon</label>
                        <select id="pok_type_1" name="pok_type_1" class="form-control">
                            <option selected>Selectionnez le type du Pokémon</option>
                            <?php foreach ($types as $type) { ?>
                                <option value="<?= $type->id ?>"><?= $type->typ_name ?></option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </form>
            <p class="resultat"></p>
        </div>
    </div>
</div>