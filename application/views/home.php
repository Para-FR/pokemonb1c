<!-- Begin page content -->
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center text-danger">Liste de mon Pokédex</h1>
        </div>
        <div class="col-12">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Type</th>
                    <th scope="col">HP</th>
                    <th scope="col">Poids</th>
                    <th scope="col">Taille</th>
                    <th scope="col">Evolution</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pokemons as $pokemon) { ?>
                <tr>
                    <th scope="row"><?= $pokemon->id ?></th>
                    <td><?= $pokemon->pok_name ?></td>
                    <td><?= $pokemon->typ_name ?></td>
                    <td><?= $pokemon->pok_hp ?></td>
                    <td><?= $pokemon->pok_weight ?></td>
                    <td><?= $pokemon->pok_height ?></td>
                    <td><?= $pokemon->pok_evol_id ?></td>
                    <td><a target="_blank" href="<?= base_url('pokemon/profile/') . $pokemon->id ?>" class="btn btn-primary">Découvrir</a></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
