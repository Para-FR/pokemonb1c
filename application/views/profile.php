<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card mx-auto mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= $pokemon->pok_img_url ?>" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $pokemon->pok_name ?></h5>
                            <p class="card-text"><?= $pokemon->pok_description ?></p>
                            <p class="card-text"><small class="text-muted">Type du Pokémon : </small>
                                <img src="<?= $pokemon->typ_img_url ?>" alt="...">
                                <?php if ($pokemon->typ_name2 && $pokemon->typ_img_url2) { ?>
                                    <img src="<?= $pokemon->typ_img_url2 ?>" alt="...">
                                <?php } ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>