<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pokemon_model', 'pokemonManager');
    }

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    $this->data['pokemons'] = $this->pokemonManager->getPokemons('p.id, p.pok_name, pt.typ_name, p.pok_hp, p.pok_weight, p.pok_height, p.pok_evol_id');
		$this->data['subview'] = 'home';
		//die(var_dump($this->data));
	    $this->load->view('components_front/main', $this->data);
	}

	public function pokemonCard($id) {

	    $this->data['pokemon'] = $this->pokemonManager->getPokemons('p.id, p.pok_name, p.pok_description, pt.typ_name, pt2.typ_name AS typ_name2 , pt.typ_img_url, pt2.typ_img_url AS typ_img_url2 , p.pok_img_url', 'p.id', $id, 'row');
        $this->data['subview'] = 'profile';
        //die(var_dump($this->data));
        $this->load->view('components_front/main', $this->data);

    }

    public function addPokemonCard() {

	    $this->data['types'] = $this->pokemonManager->getPokemonType();
	    //die(var_dump($this->data['types']));
	    $this->data['subview'] = 'addPokemon';
	    $this->load->view('components_front/main', $this->data);

    }

    public function formValidation() {
	    //die(var_dump($this->input->post()));

        $arrayRules = array(
            array(
                'field' => 'pok_name',
                'label' => 'Nom du Pokémon',
                'rules' => 'required'
            ),
            array(
                'field' => 'pok_hp',
                'label' => 'HP du Pokémon',
                'rules' => 'required'
            ),
            array(
                'field' => 'pok_description',
                'label' => 'Description du Pokémon',
                'rules' => 'required'
            ),
            array(
                'field' => 'pok_img_url',
                'label' => 'Lien de l\'image du Pokémon',
                'rules' => 'required'
            ),
            array(
                'field' => 'pok_type_1',
                'label' => 'Type du Pokémon',
                'rules' => 'required'
            ),
        );

        $this->form_validation->set_rules($arrayRules);

        if ($this->form_validation->run() === FALSE) {

            echo 'Error';

        } else {

            //echo 'Success';
            $content['pok_name'] = $this->input->post('pok_name');
            $content['pok_hp'] = $this->input->post('pok_hp');
            $content['pok_description'] = $this->input->post('pok_description');
            $content['pok_img_url'] = $this->input->post('pok_img_url');
            $content['pok_type_1'] = $this->input->post('pok_type_1');

            $this->db->insert('pokemons', $content);

            if ($this->db->affected_rows() > 0) {

                header('Content-type:application/json');
                echo json_encode(array(
                    'success' => 'Votre pokémon a été ajouté avec succès !'
                ));

            } else {
                header('Content-type:application/json');
                echo json_encode(array(
                    'error' => 'Un problème est survenu lors de l\'ajout du Pokémon'
                ));
            }



        }

    }
}
