<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pokemon_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getPokemons($select, $where = null, $value = null, $returnType = 'array'){
    $this->db->select($select)
        ->from('pokemons p')
        ->join('poke_type pt', 'pt.id = p.pok_type_1')
        ->join('poke_type pt2', 'pt2.id = p.pok_type_2', 'LEFT')
        ->order_by('p.id', 'ASC');

    if ($where != null && $value != null) {
        $this->db->where($where, $value);
    }

    $pokemons = $this->db->get();

    if ($returnType === 'row') {
        //die(var_dump($pokemons->row()));
        return $pokemons->row();
    } else {
        return $pokemons->result();
    }
    }

    public function getPokemonType()
    {

        $this->db->select('id, typ_name')
            ->from('poke_type');

        $types = $this->db->get();

        return $types->result();
    }

}
